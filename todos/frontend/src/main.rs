use todos_client::{
    frontend::{use_query, use_request, CacheProvider},
};
use yew::prelude::*;
use yew_router::prelude::*;
use uuid::Uuid;

#[function_component]
fn Home() -> Html {
    html! {}
}

#[derive(Properties, PartialEq)]
pub struct ListProps {
    list: Uuid,
}

#[function_component]
fn List(props: &ListProps) -> Html {
    html! {}
}

#[derive(Debug, Clone, Copy, PartialEq, Routable)]
pub enum Route {
    #[at("/")]
    Home,

    #[at("/list/:list")]
    List {
        list: Uuid,
    }
}

impl Route {
    fn render(self) -> Html {
        match self {
            Route::Home => html! { <Home /> },
            Route::List { list } => html! { <List {list} /> },
        }
    }
}

#[function_component]
fn App() -> Html {
    html! {
        <CacheProvider>
            <BrowserRouter>
                <Switch<Route> render={Route::render} />
            </BrowserRouter>
        </CacheProvider>
    }
}

fn main() {
    wasm_logger::init(Default::default());
    yew::Renderer::<App>::new().render();
}
