use apply::Apply;
use axum::{
    extract::{
        ws::{Message, WebSocket, WebSocketUpgrade},
        Path, State,
    },
    response::Response,
    routing::{get, post},
    Json, Router, Server,
};
use clap::Parser;
use std::{
    collections::BTreeMap,
    net::SocketAddr,
    sync::{
        atomic::{AtomicU64, Ordering},
        Arc,
    },
};
use todos_client::*;
use tokio::sync::{broadcast::*, RwLock};
use uuid::Uuid;

/// Command-line options.
#[derive(Parser, Debug)]
pub struct Options {
    /// Address and port to listen to for incoming connections.
    #[clap(long, short, default_value = "0.0.0.0:8000")]
    listen: SocketAddr,
}

/// Entry for a todo-list.
#[derive(Debug, Clone)]
pub struct TaskEntry {
    pub order: Vec<Uuid>,
    pub name: String,
    pub items: BTreeMap<Uuid, Item>,
}

/// Shared state.
#[derive(Clone, Debug)]
pub struct Backend {
    /// Todo lists and items.
    lists: Arc<RwLock<BTreeMap<Uuid, Arc<RwLock<TaskEntry>>>>>,
    /// Broadcast queue for events.
    events: Sender<Mutation>,
}

async fn list_create(State(state): State<Backend>, Json(request): Json<ListCreate>) {
    let mut lists = state.lists.write().await;
    lists.entry(request.list).or_insert(
        TaskEntry {
            order: vec![],
            name: request.name.as_deref().unwrap_or("New List").to_string(),
            items: Default::default(),
        }
        .apply(RwLock::new)
        .apply(Arc::new),
    );
    drop(lists);
    state.events.send(Mutation::List { list: request.list });
}

async fn handler() {}

async fn list_get(State(state): State<Backend>, Path(list): Path<Uuid>) -> Result<Json<List>, ()> {
    let lists = state.lists.read().await;
    let list = match lists.get(&list) {
        Some(list) => list.clone(),
        None => return Err(()),
    };
    drop(lists);

    let list = list.read().await;
    List {
        name: list.name.clone(),
        items: list.order.clone(),
    }
    .apply(Json)
    .apply(Ok)
}

async fn list_edit(State(state): State<Backend>, Path(list): Path<Uuid>) {
}

async fn list_delete(State(state): State<Backend>, Path(list): Path<Uuid>) {
    let mut lists = state.lists.write().await;
    lists.remove(&list);
}

async fn item_create(State(state): State<Backend>, Path(list): Path<Uuid>) {
}

async fn item_edit(State(state): State<Backend>, Path(list): Path<Uuid>) {
}

async fn item_delete(State(state): State<Backend>, Path(list): Path<Uuid>) {
}

async fn item_get(State(state): State<Backend>, Path(list): Path<Uuid>) -> Result<Json<Item>, ()> {
    Err(())
}

impl Backend {
    /// Create new Backend state.
    fn new() -> Self {
        Self {
            lists: Default::default(),
            events: channel(32).0,
        }
    }

    /// Setup router
    fn router(&self) -> Router {
        Router::new()
            .route("/api/v1/lists", post(list_create))
            .route(
                "/api/v1/list/:list",
                get(list_get).patch(list_edit).delete(list_delete),
            )
            .route(
                "/api/v1/list/:list/item/:item",
                get(item_get).patch(item_edit).delete(item_delete),
            )
            .with_state(self.clone())
    }
}

#[tokio::main]
async fn main() {
    // parse command-line options
    let options = Options::parse();

    // initialize tracing
    tracing_subscriber::fmt::init();

    // initialize shared state
    let backend = Backend::new();
    let router = backend.router();

    // listen for incoming connections
    Server::bind(&options.listen)
        .serve(router.into_make_service())
        .await
        .unwrap();
}
