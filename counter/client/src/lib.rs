use restless::{data::*, methods::*, *};
use serde::*;
use std::borrow::Cow;
use wasm_cache::Invalidatable;

/// Get current counter value.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetCounter;

/// Increment counter value.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct IncrementCounter;

/// Cache invalidation kinds.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(tag = "kind", rename_all = "snake_case")]
pub enum Mutation {
    /// Counter value invalid.
    Counter,
}

/// Current counter value.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CounterValue {
    pub value: u64,
}

impl GetRequest for GetCounter {
    type Response = Json<CounterValue>;
    type Query = ();

    fn path(&self) -> Cow<'_, str> {
        "api/v1/counter".into()
    }

    fn query(&self) {}
}

impl RequestMethod for GetCounter {
    type Method = Get<Self>;
}

impl Invalidatable<Mutation> for GetCounter {}

impl PostRequest for IncrementCounter {
    type Request = ();

    fn path(&self) -> Cow<'_, str> {
        "api/v1/counter".into()
    }

    fn body(&self) {}
}

impl RequestMethod for IncrementCounter {
    type Method = Post<Self>;
}

#[cfg(feature = "frontend")]
pub mod frontend {
    use super::Mutation;
    use restless::clients::{gloo::*, wasm_cache::CachedRequest, yew::*};
    use std::{fmt::Debug, hash::Hash};
    use wasm_cache::{
        yew::{Cache, CacheProvider as WasmCache},
        Invalidatable, RcValue,
    };
    use yew::prelude::*;

    #[hook]
    pub fn use_request<R: GlooRequest + 'static, F: Fn(&Result<R::Response, R::Error>) + 'static>(
        request: R,
        after: F,
    ) -> UseRequestHandle<R::Response, R::Error>
    where
        R::Response: Clone + 'static,
    {
        restless::clients::yew::use_request("https://counter.api.rustutils.com/".into(), request, after, false)
    }

    #[hook]
    pub fn use_query<
        R: GlooRequest + Invalidatable<Mutation> + Clone + Ord + Eq + Hash + Debug + 'static,
    >(
        request: R,
    ) -> RcValue<R::Response>
    where
        R::Response: Clone + 'static + PartialEq + Debug,
    {
        wasm_cache::yew::use_cached::<Mutation, _>(CachedRequest {
            prefix: "https://counter.api.rustutils.com/".into(),
            request,
        })
    }

    #[derive(Properties, PartialEq)]
    pub struct CacheProviderProps {
        pub children: Children,
    }

    #[function_component]
    pub fn CacheProvider(props: &CacheProviderProps) -> Html {
        let cache = Cache::<Mutation>::default();
        cache.websocket_listener("wss://counter.api.rustutils.com/api/v1/events");
        html! {
            <WasmCache<Mutation> {cache}>
                { for props.children.iter() }
            </WasmCache<Mutation>>
        }
    }
}
