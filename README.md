# Yew Workshop

This repository contains backend code used for my Yew workshop. The backends are built with
Axum and contain API clients that are readily consumable from within Yew.

## License

MIT.
