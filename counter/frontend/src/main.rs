use counter_client::{
    frontend::{use_query, use_request, CacheProvider},
    GetCounter, IncrementCounter,
};
use yew::prelude::*;

#[function_component]
fn CounterCard() -> Html {
    let counter = use_query(GetCounter);
    let increment = use_request(IncrementCounter, |_| ());

    html! {
        <div class="max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{"Counter"}</h5>
            <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">{"Current value: "}
                if let Some(data) = counter.data() {
                    {data.value.to_string()}
                } else {
                    {"Loading..."}
                }
            </p>
            <button class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" onclick={move |_| increment.run()}>
                {"Increment"}
                <svg class="w-3.5 h-3.5 ml-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
                </svg>
            </button>
        </div>
    }
}

#[function_component]
fn Counter() -> Html {
    let counter = use_query(GetCounter);
    let increment = use_request(IncrementCounter, |_| ());

    html! {
        <div class="max-w-xl m-auto p-4">
            <h1 class="text-xl font-bold py-3">{"Hello, World!"}</h1>
            <CounterCard />
        </div>
    }
}

#[function_component]
fn App() -> Html {
    html! {
        <CacheProvider>
            <Counter />
        </CacheProvider>
    }
}

fn main() {
    wasm_logger::init(Default::default());
    yew::Renderer::<App>::new().render();
}
