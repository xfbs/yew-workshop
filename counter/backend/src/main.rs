use axum::{
    extract::{
        ws::{Message, WebSocket, WebSocketUpgrade},
        State,
    },
    response::Response,
    routing::get,
    Json, Router, Server,
};
use clap::Parser;
use counter_client::*;
use std::{
    net::SocketAddr,
    sync::{
        atomic::{AtomicU64, Ordering},
        Arc,
    },
};
use tokio::sync::broadcast::*;
use tower_http::cors::CorsLayer;

/// Command-line options.
#[derive(Parser, Debug)]
pub struct Options {
    /// Address and port to listen to for incoming connections.
    #[clap(long, short, default_value = "0.0.0.0:8000")]
    listen: SocketAddr,

    /// Initial value of counter.
    #[clap(long, short, default_value = "0")]
    counter: u64,

    /// Size of broadcast channel.
    #[clap(long, default_value = "32")]
    channel_size: usize,
}

impl Default for Options {
    fn default() -> Self {
        Self {
            channel_size: 32,
            listen: "0.0.0.0:8000".parse().unwrap(),
            counter: 0,
        }
    }
}

/// Shared state.
#[derive(Clone, Debug)]
pub struct Backend {
    /// Current counter value.
    counter: Arc<AtomicU64>,
    /// Broadcast queue for events.
    events: Sender<Mutation>,
}

impl Backend {
    /// Create new Backend state.
    fn new(options: &Options) -> Self {
        Self {
            counter: Arc::new(options.counter.into()),
            events: channel(options.channel_size).0,
        }
    }

    /// Setup router
    fn router(&self) -> Router {
        Router::new()
            .route("/api/v1/counter", get(counter_get).post(counter_increment))
            .route("/api/v1/events", get(events))
            .with_state(self.clone())
            .layer(CorsLayer::permissive())
    }
}

/// Get current counter value.
async fn counter_get(State(backend): State<Backend>) -> Json<CounterValue> {
    Json(CounterValue {
        value: backend.counter.load(Ordering::Relaxed),
    })
}

/// Increment current counter value.
async fn counter_increment(State(backend): State<Backend>) {
    backend.counter.fetch_add(1, Ordering::Relaxed);
    let _ = backend.events.send(Mutation::Counter);
}

/// Handle event WebSocket connections.
async fn events(State(backend): State<Backend>, ws: WebSocketUpgrade) -> Response {
    let mut events = backend.events.subscribe();
    ws.on_upgrade(|mut websocket: WebSocket| async move {
        loop {
            match events.recv().await {
                Ok(invalidation) => {
                    let encoded = serde_json::to_string(&invalidation).unwrap();
                    if let Err(error) = websocket.send(Message::Text(encoded)).await {
                        tracing::error!("{error:?}");
                        return;
                    }
                }
                Err(error) => {
                    tracing::error!("{error:?}");
                }
            }
        }
    })
}

#[tokio::main]
async fn main() {
    // parse command-line options
    let options = Options::parse();

    // initialize tracing
    tracing_subscriber::fmt::init();

    // initialize shared state
    let backend = Backend::new(&options);
    let router = backend.router();

    // listen for incoming connections
    Server::bind(&options.listen)
        .serve(router.into_make_service())
        .await
        .unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;
    use restless::clients::HyperRequest;
    use test_strategy::proptest;
    use tokio::task::JoinSet;
    use tower::ServiceExt;

    #[proptest(async = "tokio")]
    async fn counter_get(counter: u64) {
        let options = Options {
            counter,
            ..Default::default()
        };
        let backend = Backend::new(&options);
        let router = backend.router();

        let _response = router
            .oneshot(GetCounter.to_hyper_request().unwrap())
            .await
            .unwrap();
    }

    #[proptest(async = "tokio")]
    async fn counter_increment(counter: u64, amount: u8) {
        let options = Options {
            counter,
            ..Default::default()
        };
        let backend = Backend::new(&options);

        // launch all requests in parallel
        let mut join_set = JoinSet::new();
        for _ in 0..amount {
            let router = backend.router();
            join_set.spawn(async move {
                router
                    .oneshot(IncrementCounter.to_hyper_request().unwrap())
                    .await
                    .unwrap();
            });
        }

        // wait for all requests to complete
        while let Some(_task) = join_set.join_next().await {}

        // ensure that the counter was incremented an appropriate number of times
        assert_eq!(
            backend.counter.load(Ordering::Relaxed),
            counter + amount as u64
        );
    }
}
