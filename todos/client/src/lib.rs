use restless::{data::Json, methods::*, *};
use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use uuid::Uuid;

/// Create new list with the given UUID.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ListCreate {
    /// List Uuid to create.
    pub list: Uuid,
    /// Name of list
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
}

impl PostRequest for ListCreate {
    type Request = Json<Self>;

    fn path(&self) -> Cow<'_, str> {
        "api/v1/lists".into()
    }

    fn body(&self) -> Self::Request {
        Json(self.clone())
    }
}

impl RequestMethod for ListCreate {
    type Method = Post<Self>;
}

/// Get items in this list.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ListGet {
    /// List to get items for.
    pub list: Uuid,
}

impl GetRequest for ListGet {
    type Query = ();
    type Response = Json<List>;

    fn path(&self) -> Cow<'_, str> {
        format!("api/v1/list/{}", self.list).into()
    }

    fn query(&self) {}
}

impl RequestMethod for ListGet {
    type Method = Get<Self>;
}

/// Edit list.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ListEdit {
    /// List to get items for.
    pub list: Uuid,
    /// Change list name.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
}

/// Create list item.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ItemCreate {
    pub list: Uuid,
    pub item: Uuid,
    pub text: String,
}

/// Edit list item.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ItemEdit {
    pub list: Uuid,
    pub item: Uuid,
    pub text: Option<String>,
    pub done: Option<bool>,
    pub pos: Option<u64>,
}

/// Delete list item.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ItemDelete {
    pub list: Uuid,
    pub item: Uuid,
}

impl DeleteRequest for ItemDelete {
    type Query = ();

    fn path(&self) -> Cow<'_, str> {
        format!("api/v1/list/{}/item/{}", self.list, self.item).into()
    }

    fn query(&self) {}
}

impl RequestMethod for ItemDelete {
    type Method = Delete<Self>;
}

/// Get list item.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ItemGet {
    pub list: Uuid,
    pub item: Uuid,
}

impl GetRequest for ItemGet {
    type Response = Json<Item>;
    type Query = ();

    fn path(&self) -> Cow<'_, str> {
        format!("api/v1/list/{}/item/{}", self.list, self.item).into()
    }

    fn query(&self) {}
}

impl RequestMethod for ItemGet {
    type Method = Get<Self>;
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Item {
    pub item: Uuid,
    pub text: String,
    pub pos: u64,
    pub done: bool,
}

/// To-Do list, has name and items.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct List {
    pub name: String,
    pub items: Vec<Uuid>,
}

/// Cache invalidation kinds.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(tag = "kind", rename_all = "snake_case")]
pub enum Mutation {
    /// List has changed (deleted or created)
    List { list: Uuid },
    /// List items have changed (deleted, created, reordered)
    ListItems { list: Uuid },
    /// Specific list item has changed
    ListItem { list: Uuid, item: Uuid },
}

#[cfg(feature = "frontend")]
pub mod frontend {
    use super::Mutation;
    use restless::clients::{gloo::*, wasm_cache::CachedRequest, yew::*};
    use std::{fmt::Debug, hash::Hash};
    use wasm_cache::{
        yew::{Cache, CacheProvider as WasmCache},
        Invalidatable, RcValue,
    };
    use yew::prelude::*;

    #[hook]
    pub fn use_request<R: GlooRequest + 'static, F: Fn(&Result<R::Response, R::Error>) + 'static>(
        request: R,
        after: F,
    ) -> UseRequestHandle<R::Response, R::Error>
    where
        R::Response: Clone + 'static,
    {
        restless::clients::yew::use_request("https://todos.api.rustutils.com/".into(), request, after, false)
    }

    #[hook]
    pub fn use_query<
        R: GlooRequest + Invalidatable<Mutation> + Clone + Ord + Eq + Hash + Debug + 'static,
    >(
        request: R,
    ) -> RcValue<R::Response>
    where
        R::Response: Clone + 'static + PartialEq + Debug,
    {
        wasm_cache::yew::use_cached::<Mutation, _>(CachedRequest {
            prefix: "https://todos.api.rustutils.com/".into(),
            request,
        })
    }

    #[derive(Properties, PartialEq)]
    pub struct CacheProviderProps {
        pub children: Children,
    }

    #[function_component]
    pub fn CacheProvider(props: &CacheProviderProps) -> Html {
        let cache = Cache::<Mutation>::default();
        cache.websocket_listener("wss://todos.api.rustutils.com/api/v1/events");
        html! {
            <WasmCache<Mutation> {cache}>
                { for props.children.iter() }
            </WasmCache<Mutation>>
        }
    }
}
